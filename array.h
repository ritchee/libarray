#ifndef _ARRAY_H
#define _ARRAY_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>

/*********************************************************
 * uncomment the following line if using a shared library,
 * or define it before including this header
 *********************************************************/
/* #define ARRAY_SHARED */

/**************************************************************************
 * NOTE: 
 * the dynamic array was written with performance in mind, so error 
 * checkings barely exist, especially inline functions. In fact, they only
 * check legit arguments. 
 * For example: 
 * len = 0 is valid and legit, while
 * len = array.len + 1 is valid(at least in C) but not legit.
 *
 * also, it is encouraged to integrate libarray to your internal projects
 * more than to use it as a library due to its unsafe interface.
 *
 * it is the programmer's responsibility to check arguments before 
 * calling any of this functions.
 *
 * functions that insert data will return NULL if there's not enough
 * memory, in other words, ARRAY_ALLOC returns NULL, or capacity cannot
 * be extended, in which case, errno will be set to EOVERFLOW.
 *
 * macros:
 *
 * ARRAY_MAX
 * ARRAY_ALLOC
 * ARRAY_DEALLOC
 * ARRAY_FREE
 *
 * ARRAY_SLICE(array, ind, len)
 * ARRAY_CAST(array, index, type)
 * ARRAY_DEREF(array, index, type)
 * ARRAY_QPUSH(array, type)
 * ARRAY_QPOP(array, type)
 * ARRAY_FOR(array, i)
 * ARRAY_RANGE(array, i, from, to)
 * ARRAY_RFOR(array, i)
 * ARRAY_RRANGE(array, i, from, to)
 * ARRAY_ITER(array, iterator, type)
 * ARRAY_QITER(array, iterator, type)
 *
 * There are also macros which are the upper case of each prototype that
 * takes a whole array instead of a slice for saving typing time.
 *
 * prototypes:
 *
 * struct array* array_new(const void *data, size_t size, array_ui len);
 * struct array* array_init(struct array *array, size_t size, array_ui cap);
 * void* array_insert(struct array *array, const void *data, 
 *			array_ui ind, array_ui dlen);
 * void* array_sinsert(struct array *array, const void *data,
 *			array_ui ind, array_ui len, 
 *			int (*comp_fn)(const void*, const void*));
 * void* array_hpush(struct array *array, const void *data,
 *			array_ui ind, array_ui len, 
 *			int (*comp_fn)(const void*, const void*));
 * void* array_hpop(struct array *array, array_ui ind, array_ui len, 
 *			int (*comp_fn)(const void*, const void*));
 * void array_delete(struct array *array, array_ui ind, array_ui len);
 * array_ui array_remove(struct array *array, array_ui ind, array_ui len);
 * void array_swap(struct array *array, array_ui ind1, array_ui ind2,
 *			array_ui len);
 * struct array* array_copy(struct array *array, array_ui ind, array_ui len, 
 *			void (*copy_fn)(void*, const void*));
 * void* array_join(struct array *array, array_ui ind, 
 *			struct array *to_join);
 * array_ui array_interpose(struct array *array, array_ui ind, 
 *			array_ui len, array_sl offset); DEPRECATED
 * void array_rotate(struct array *array, array_ui ind, array_ui len,
 *			array_sl offset);
 * struct array* array_splice(struct array *array, array_ui ind, array_ui len, 
 *			struct array *dest);
 * void array_shuffle(struct array *array, array_ui ind, array_ui len);
 * void array_reverse(struct array *array, array_ui ind, array_ui len);
 * void* array_resize(struct array *array, array_ui new_cap);
 * void* array_extend(struct array *array, array_ui len);
 * void array_sort(struct array *array, array_ui ind, array_ui len, 
 *			int (*comp_fn)(const void*, const void*));
 * void array_ssort(struct array *array, array_ui ind, array_ui len, 
 *			int (*comp_fn)(const void*, const void*));
 * array_ui array_partition(struct array *array, array_ui ind, 
 *			array_ui len, int (*pred_fn)(const void*));
 * array_ui array_spartition(struct array *array, array_ui ind, 
 *			array_ui len, int (*pred_fn)(const void*));
 * void array_dedup(struct array *array, array_ui ind, array_ui len,
 *			int (*comp_fn)(const void*, const void*));
 * array_ui array_lbound(struct array *array, const void *data, 
 *			array_ui ind, array_ui len,
 *			int (*comp_fn)(const void*, const void*));
 * array_ui array_ubound(struct array *array, const void *data, 
 *			array_ui ind, array_ui len,
 *			int (*comp_fn)(const void*, const void*));
 * array_ui array_bsearch(struct array *array, const void *data, 
 *			array_ui ind, array_ui len, 
 *			int (*comp_fn)(const void*, const void*));
 * array_ui array_lsearch(struct array *array, const void *data, 
 *			array_ui ind, array_ui len, 
 *			int (*comp_fn)(const void*, const void*));
 * array_ui array_find(struct array *array, array_ui ind, array_ui len, 
 *			int (*pred_fn)(const void*));
 * array_ui array_rfind(struct array *array, array_ui ind, array_ui len, 
 *			int (*pred_fn)(const void*));
 * void array_write(struct array *array, const void *data, array_ui ind, 
 *			array_ui len);
 * void array_rwrite(struct array *array, const void *data, array_ui ind, 
 *			array_ui len);
 * int array_sorted(struct array *array, array_ui ind, array_ui len,
 *			int (*comp_fn)(const void*, const void*));
 * void array_clear(struct array *array);
 * void array_free(struct array *array);
 * void array_print(struct array *array, array_ui ind, 
 *			array_ui len, const char *delimiter,
 *			void (*display)(const void *));
 * ARRAY_INLINE void* array_append(struct array *array, const void *data);
 * ARRAY_INLINE void* array_push(struct array *array);
 * ARRAY_INLINE void* array_pop(struct array *array);
 * ARRAY_INLINE void* array_reserve(struct array *array, array_ui len);
 * ARRAY_INLINE struct array* array_dup(struct array *array, 
 *			void (*copy_fn)(void*, const void*));
 * ARRAY_INLINE void* array_shrink(struct array *array);
 * ARRAY_INLINE void array_trunc(struct array *array);
 * ARRAY_INLINE void array_erase(struct array *array, array_ui ind);
 * ARRAY_INLINE void array_lrotate(struct array *array, array_ui ind, 
 *			array_ui len, array_ui offset);
 * ARRAY_INLINE void array_rrotate(struct array *array, array_ui ind,
 *			array_ui len, array_ui offset);
 * ARRAY_INLINE void* array_get(struct array *array, array_ui ind);
 * ARRAY_INLINE array_ui array_index(struct array *array, 
 *			const void *datap);
 * ARRAY_INLINE array_ui array_vacancy(struct array *array);
 * ARRAY_INLINE void* array_data(struct array *array);
 * ARRAY_INLINE size_t array_size(struct array *array);
 * ARRAY_INLINE array_ui array_len(struct array *array);
 * ARRAY_INLINE array_ui array_cap(struct array *array);
 * ARRAY_INLINE array_ui array_end(struct array *array, array_ui ind);
 * ARRAY_INLINE array_ui array_rind(struct array *array, array_ui rind);
 * ARRAY_INLINE int array_full(struct array *array);
 * ARRAY_INLINE int array_empty(struct array *array);
 *************************************************************************/


#define ARRAY_INLINE static inline __attribute__((always_inline))

/* namespace types */
/* 32 bit int type is intended for minimalizing storage and overflow */
typedef uint32_t array_ui;
typedef int32_t array_si;
typedef uint64_t array_ul;
typedef int64_t array_sl;
typedef unsigned char array_uc;

/**
 * |-------------------------- cap ----------------------------|
 * |----------------- len ------------------|                  |
 * | size|                                  |                  |
 * +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 * +  0  +  1  +  ............  +  len - 1  +  ... unused ...  +
 * +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 * ^
 * data
 */

/**
 * struct array: dynamic array
 *
 * @data:	pointer to array's internal data block
 * @len: number of elements
 * @cap: array's capacity which should be greater or equal to len
 * @size:	size of each element
 */
struct array {
	void *data;
	array_ui len;
	array_ui cap;
	size_t size;
};

/**
 * maximum possible length for an array, an index value should not reach
 * this
 */
#define ARRAY_MAX (~0)

/**
 * custom memory allocation macros
 *
 * defaulted to stdlib's functions
 *
 * these macros should be overriden with custom allocation functions if
 * necessary.
 */
#define ARRAY_ALLOC(s) malloc(s)
#define ARRAY_REALLOC(p, s) realloc(p, s)
#define ARRAY_FREE(p) free(p)

#define ARRAY_SLICE(a, i, l) (struct array { \
		.data = array_get(a, i), \
		.size = (a)->size, \
		.len = len, \
		.cap = len, \
		})

/* cast the returned pointer to pointer of the passed type */
#define ARRAY_CAST(a, i, t) ((t*)array_get(a, i))

/* cast and dereference the returned pointer to the passed type */
#define ARRAY_DEREF(a, i, t) (*(t*)array_get(a, i))

/**
 * quick push and pop macros 
 * example:
 * ARRAY_QPUSH(&some_array, int) = 4;
 * int n = ARRAY_QPOP(&some_array, int);
 */
#define ARRAY_QPUSH(a, t) (*(t*)array_push(a))
#define ARRAY_QPOP(a, t) (*(t*)array_pop(a))

/**
 * these MACROS are useful for cleanup or any necessary change to an item
 *
 * loop through every element using a custom index
 * example: ARRAY_FOR(&array, n) { f(array_get(&array, n); }
 */
#define ARRAY_FOR(ap, ind) \
	for (array_ui ind = 0; ind < (ap)->len; ind++)
/* ARRAY_FOR with a defined range */
#define ARRAY_RANGE(ap, ind, from, to) \
	for (array_ui ind = from; ind < to; ind++)
/* ARRAY_FOR in the reverse order */
#define ARRAY_RFOR(ap, ind) \
	for (array_sl ind = (ap)->len - 1; ind >= 0; ind--)
/* ARRAY_RFOR with a defined range */
#define ARRAY_RRANGE(ap, ind, from, to) \
	for (array_sl ind = from; ind >= to; ind--)
/* iterating with a pointer 
 *
 * example:
 * ARRAY_ITER(&some_array, p, int) {
 *			printf("%d ", *p);
 * }
 */
#define ARRAY_ITER(ap, it, type) \
	for (type *it = (ap)->data; \
			it < (type*)((array_uc*)((ap)->data) + (ap)->len * (ap)->size); \
			it++)
/* static iterating with a pointer
 * that is, array's data remains the same through out the iteration
 * faster than ARRAY_ITER since it doesn't compute the end iterator each loop
 *
 * example:
 * ARRAY_QITER(&some_array, p, int) {
 *			printf("%d ", *p);
 * }
 */
#define ARRAY_QITER(ap, it, type) \
	for (type *it = (ap)->data, \
			*rit = (type*)((array_uc*)((ap)->data) + (ap)->len * (ap)->size);\
			it < rit; \
			it++)

/* macro that convert an entire array into slice */
/* only functions that take slice have a corresponding macro */
#define ARRAY_SINSERT(a, d, fn) array_sinsert(a, d, 0, (a)->len, fn)
#define ARRAY_HPUSH(a, d, fn) array_hpush(a, d, 0, (a)->len, fn)
#define ARRAY_HPOP(a, d, fn) array_hpop(a, d, 0, (a)->len, fn)
#define ARRAY_DELETE(a) array_trunc(a)
#define ARRAY_COPY(a, fn) array_dup(a, fn)
#define ARRAY_ROTATE(a, os) array_rotate(a, 0, (a)->len, os)
#define ARRAY_SHUFFLE(a) array_shuffle(a, 0, (a)->len)
#define ARRAY_REVERSE(a) array_reverse(a, 0, (a)->len)
#define ARRAY_SORT(a, fn) array_sort(a, 0, (a)->len, fn)
#define ARRAY_SSORT(a, fn) array_ssort(a, 0, (a)->len, fn)
#define ARRAY_PARTITION(a, fn) array_partition(a, 0, (a)->len, fn)
#define ARRAY_SPARTITION(a, fn) array_spartition(a, 0, (a)->len, fn)
#define ARRAY_DEDUP(a) array_dedup(a, 0, (a)->len)
#define ARRAY_LBOUND(a, d, fn) array_lbound(a, d, 0, (a)->len, fn)
#define ARRAY_UBOUND(a, d, fn) array_ubound(a, d, 0, (a)->len, fn)
#define ARRAY_BSEARCH(a, d, fn) array_bsearch(a, d, 0, (a)->len, fn)
#define ARRAY_LSEARCH(a, d, fn) array_lsearch(a, d, 0, (a)->len, fn)
#define ARRAY_FIND(a, fn) array_find(a, 0, (a)->len)
#define ARRAY_RFIND(a, fn) array_rfind(a, 0, (a)->len)
#define ARRAY_WRITE(a, d) array_write(a, d, 0, (a)->len)
#define ARRAY_RWRITE(a, d) array_rwrite(a, d, 0, (a)->len)
#define ARRAY_SORTED(a, fn) array_sorted(a, 0, (a)->len, fn)
#define ARRAY_PRINT(a, delim, fn) array_print(a, 0, (a)->len, delim, fn)
#define ARRAY_LROTATE(a, os) array_lrotate(a, 0, (a)->len, os)
#define ARRAY_RROTATE(a, os) array_rrotate(a, 0, (a)->len, os)

/**
 * array_new
 *
 * @brief: returns a newly allocated array for the given data
 *
 * @data: data to be copied
 * @size: size of an element
 * @len: number of elements
 *
 * this function is equivalent to this piece of code:
 * struct array *a = ARRAY_ALLOC(sizeof(struct array));
 * array_init(a, size, len);
 * array_insert(a, data, 0, len);
 *
 * if @data is NULL. It will be as if array_insert was never 
 * called.
 *
 * array_init() must still be called with any static or local array
 *
 * returns a newly allocated and initialized array, or NULL if no memory or
 *         array_init failed.
 */
struct array* array_new(const void *data, size_t size, array_ui len);

/**
 * array_init
 *
 * @brief: initilizes an array
 *
 * @array: pointer to array 
 * @size: size of an element
 * @cap: capacity of the newly allocated block
 *
 * array_init initializes @array->size to @size and @array->cap to @cap, 
 * allocates a new block with size of @size * @cap.
 *
 * returns @array if successful, NULL if size or cap is 0, or no memory.
 */
struct array* array_init(struct array *array, size_t size, 
		array_ui cap);

/**
 * array_insert
 *
 * @brief: inserts items at a specified index
 *
 * @array: pointer to array 
 * @data: items that need inserting
 * @ind: index at which to insert
 * @dlen: number of items to be inserted
 *
 * array_insert inserts a number of @dlen items specified in @data 
 * at @array's index @ind.
 *
 * if @data is NULL, the insert space will be zero'd out.
 *
 * in case @len is 0, it's still considered successful.
 *
 * returns a pointer to the newly inserted block if successful, 
 *         NULL if no memory.
 */
void* array_insert(struct array *array, const void *data, 
		array_ui ind, array_ui dlen);

/**
 * array_sinsert
 *
 * @brief: inserts items in a sorted order
 *
 * @array: pointer to array 
 * @data: pointer to the item that needs inserting
 * @ind: index at which to calibrate
 * @len: range
 * @comp_fn: compare function
 *
 * array_sinsert inserts an item and sorts everything on the fly
 * 
 * NOTE: items within the specified range MUST be sorted 
 *       and @data cannot be NULL, in contrast to array_insert.
 *
 * returns a pointer to the newly inserted item, NULL if no memory.
 */
void* array_sinsert(struct array *array, const void *data, 
		array_ui ind, array_ui len, int (*comp_fn)(const void*, const void*));

/**
 * array_hpush
 *
 * @brief: inserts items into a heap 
 *
 * @array: pointer to array (as a heap)
 * @data: pointer to the item that needs inserting
 * @ind: index at which to calibrate
 * @len: range
 * @comp_fn: compare function
 *
 * array_hpush inserts an item into an array that is a heap of @len 
 * items at index @ind.
 * the heap is assumed to be a max heap, that is, the larger item 
 * according to @comp_fn will be higher in the heap
 * 
 * NOTE: items within the specified range MUST be a heap 
 *       and @data cannot be NULL.
 *
 * returns a pointer to the newly inserted item, NULL if no memory.
 */
void* array_hpush(struct array *array, const void *data, array_ui ind,
		array_ui len, int (*comp_fn)(const void*, const void*));

/**
 * array_hpop
 *
 * @brief: pop the top of the heap
 *
 * @array: pointer to array (as a heap)
 * @rdata: pointer to where the popped item should be
 * @ind: index at which to calibrate
 * @len: range
 * @comp_fn: compare function
 *
 * array_hpop pops the top item of the heap and reorders items within
 * the heap tree.
 * this item should be at index @ind unless @len is 0.
 * @rdata should be where the popped item is returned, so make sure 
 * its size equals @array->size.
 * 
 * NOTE: items within the specified range MUST be a heap 
 *       and @rdata cannot be NULL.
 *
 * returns nothing.
 */
void array_hpop(struct array *array, void *rdata, array_ui ind,
		array_ui len, int (*comp_fn)(const void*, const void*));

/**
 * array_delete
 *
 * @brief: deletes items at a specified index
 *
 * @array: pointer to array
 * @ind: index at which to delete
 * @len: number of items
 *
 * array_delete deletes (not removes) a number of @len items at index @ind
 *
 * returns nothing.
 */
void array_delete(struct array *array, array_ui ind, 
		array_ui len);

/**
 * array_remove
 *
 * @brief: removes items at a specified index
 *
 * @array: pointer to array
 * @ind: index at which to remove
 * @len: number of items
 *
 * array_remove removes a number of @len items at index @ind to the end
 * of @array, @array's length is unchanged.
 *
 * returns index of the removed items.
 */
array_ui array_remove(struct array *array, array_ui ind, 
		array_ui len);

/**
 * array_swap
 *
 * @brief: swaps items at 2 specified indices
 *
 * @array: pointer to array 
 * @ind1: first item
 * @ind2: second item
 * @len: amount of items to swap
 *
 * array_swap swaps @len items at @ind1 and @ind2
 *
 * NOTE: be mindful that the function does not check whether items overlap.    
 *
 * returns nothing.
 */
void array_swap(struct array *array, array_ui ind1, 
		array_ui ind2, array_ui len);

/**
 * array_copy
 *
 * @brief: makes a new array with items at a specified range
 *
 * @array: pointer to array 
 * @ind: index at which to copy
 * @len: number of items
 * @copy_fn: custom copy function
 *
 * array_copy copies @len number of items at index @ind into a newly 
 * allocated array using @copy_fn.
 * if @copy_fn is NULL, memcpy will be used instead.
 *
 * returns @dest if successful, NULL if no memory or @len is 0.
 */
struct array* array_copy(struct array *array, array_ui ind, 
		array_ui len, void (*copy_fn)(void*, const void*));

/**
 * array_join
 *
 * @brief: joins an array to another at a specified index
 *
 * @array: pointer to array
 * @ind: index at which to join
 * @to_join: pointer to the array that joins @array
 *
 * array_join joins @to_join with @array at index @ind and truncates
 * @to_join->len to 0.
 *
 * returns a pointer to the newly joined block if successful, 
 *         NULL if @to_join's data cannot be inserted into @array.
 */
void* array_join(struct array *array, array_ui ind, 
		struct array *to_join);

/**
 * array_interpose
 *
 * @brief: interpose items to another index
 *
 * @array: pointer to array
 * @ind: index at which to interpose
 * @len: number of items
 * @offset: interpose distance
 *
 * array_interpose moves @len items at index @ind an @offset number of 
 * times, if offset < 0, items will be moved left and vice versa.
 *
 * NOTE: offset is clamped if its absolute range is greater than ind or
 * @array->len - ind - len. This function is tricky so experiment with it
 * before putting it into real code.
 *
 * returns index of the new position of said items.
 */
array_ui array_interpose(struct array *array, array_ui ind, 
		array_ui len, array_sl offset);

/**
 * array_rotate
 *
 * @brief: rotate items within a range
 *
 * @array: pointer to array
 * @ind: index at which to rotate
 * @len: number of items
 * @offset: rotation rate
 *
 * array_rotate rotates @len number of items at index @ind an @offset 
 * number of times, if offset < 0, items will be rotated left and 
 * vice versa.
 *
 * returns nothing.
 */
void array_rotate(struct array *array, array_ui ind, 
		array_ui len, array_sl offset);

/**
 * array_splice
 *
 * @brief: removes items at a specified index into a new array
 *
 * @array: pointer to array
 * @ind: index at which to splice
 * @len: number of items
 * @dest: destination array
 *
 * array_splice removes a number of @len items at index @ind into 
 * destination array @dest at its end.
 * if @dest is NULL, then a new array is allocated and returned.
 *
 * returns @dest, NULL if no memory.
 */
struct array* array_splice(struct array *array, array_ui ind, 
		array_ui len, struct array *dest);

/**
 * array_shuffle
 *
 * @brief: shuffles items at a specified index
 *
 * @array: pointer to array
 * @ind: index at which to shuffle
 * @len: number of items
 *
 * array_shuffle shuffles a number of @len items at index @ind.
 *
 * returns nothing.
 */
void array_shuffle(struct array *array, array_ui ind, 
		array_ui len);

/**
 * array_reverse
 *
 * @brief: reverses items at a specified index
 *
 * @array: pointer to array
 * @ind: index at which to reverse
 * @len: number of items
 *
 * array_reverse reverses a number of @len items at index @ind.
 *
 * returns nothing.
 */
void array_reverse(struct array *array, array_ui ind, 
		array_ui len);

/**
 * array_resize
 *
 * @brief: resizes capacity
 *
 * @array: pointer to array
 * @new_cap: @array's resized cap
 *
 * array_resizes resize @array->data to @array->size * @new_cap,
 * @array->len is changed to @new_cap if it's less than @array->len.
 *
 * returns @array if successful, NULL if no memory or new_cap is 0.
 */
void* array_resize(struct array *array, array_ui new_cap);

/**
 * array_extend
 *
 * @brief: extend capacity 
 *
 * @array: pointer to array
 * @len: number of slots to extend
 *
 * returns @array if successful, NULL otherwise.
 */
void* array_extend(struct array *array, array_ui len);

/**
 * array_sort
 *
 * @brief: sort items at a specified index
 *
 * @array: pointer to array
 * @ind: index at which to sort
 * @len: number of items
 * @comp_fn: compare function
 *
 * array_sort sorts a number of @len items at index @ind in ascending order.
 *
 * NOTE: array_sort might not be stable sort, use array_ssort for stable-sort
 *
 * @comp_fn should returns an integer < 0, == 0, > 0 if the left item is
 * less than, equal to, or greater than the right item, respectively.
 *
 * returns nothing.
 */
void array_sort(struct array *array, array_ui ind, 
		array_ui len, int (*comp_fn)(const void*, const void*));

/**
 * array_ssort
 *
 * @brief: stable-sort items at a specified index
 *
 * @array: pointer to array
 * @ind: index at which to sort
 * @len: number of items
 * @comp_fn: compare function
 *
 * array_ssort stable-sorts a number of @len items at index @ind in
 * ascending order.
 *
 * NOTE: array_ssort uses blocksort which might be slower than array_sort,
 * so only use this if stable sort is necessary
 *
 * @comp_fn should returns an integer < 0, == 0, > 0 if the left item is
 * less than, equal to, or greater than the right item, respectively.
 *
 * returns nothing.
 */
void array_ssort(struct array *array, array_ui ind, 
		array_ui len, int (*comp_fn)(const void*, const void*));

/**
 * array_partition
 *
 * @brief: partition true items at a specified index
 *
 * @array: pointer to array
 * @ind: index at which to partition
 * @len: number of items
 * @pred_fn: predicate function
 *
 * array_partition partitions items that @pred_fn returns true from 
 * a number of @len items at index @ind, trues items will be moved to
 * the end of specified range.
 *
 * returns the index of the first partitioned item, ind + len otherwise.
 */
array_ui array_partition(struct array *array, array_ui ind, 
		array_ui len, int (*pred_fn)(const void*));

/**
 * array_spartition
 *
 * @brief: stable-partition true items at a specified index
 *
 * @array: pointer to array
 * @ind: index at which to partition
 * @len: number of items
 * @pred_fn: predicate function
 *
 * array_partition partitions items that @pred_fn returns true from 
 * a number of @len items at index @ind, trues items will be moved to
 * the end of specified range.
 *
 * returns the index of the first partitioned item, ind + len otherwise.
 */
array_ui array_spartition(struct array *array, array_ui ind, 
		array_ui len, int (*pred_fn)(const void*));

/**
 * array_dedup
 *
 * @brief: removes duplicates at a specified index
 *
 * @array: pointer to array
 * @ind: index at which to remove
 * @len: number of items
 * @comp_fn: compare function
 *
 * array_dedup removes duplicates from a number of @len items at index @ind
 *
 * @comp_fn should returns an integer < 0, == 0, > 0 if the left item is
 * less than, equal to, or greater than the right item, respectively.
 *
 * returns nothing.
 */
void array_dedup(struct array *array, array_ui ind, array_ui len,
		int (*comp_fn)(const void*, const void*));

/**
 * array_lbound
 *
 * @brief: search lower bound within the array
 *
 * @array: pointer to array
 * @data: pointer to item to be searched
 * @ind: index at which to search
 * @len: number of items
 * @comp_fn: compare function
 *
 * NOTE: THE SEARCH RANGE MUST BE SORTED!
 *
 * array_lbound searches for lower bound of the given data.
 * 
 * @comp_fn should returns an integer < 0, == 0, > 0 if the left item is
 * less than, equal to, or greater than the right item, respectively.
 *
 * returns index of the first item that is >= the given item, 
 *			   ind + len otherwise.
 */
array_ui array_lbound(struct array *array, const void *data, 
	 array_ui ind, array_ui len, int (*comp_fn)(const void*, const void*));

/**
 * array_ubound
 *
 * @brief: search upper bound within the array
 *
 * @array: pointer to array
 * @data: pointer to item to be searched
 * @ind: index at which to search
 * @len: number of items
 * @comp_fn: compare function
 *
 * NOTE: THE SEARCH RANGE MUST BE SORTED!
 *
 * array_ubound searches for upper bound of the given data.
 * 
 * @comp_fn should returns an integer < 0, == 0, > 0 if the left item is
 * less than, equal to, or greater than the right item, respectively.
 *
 * returns index of the first item that is > the given item, 
 *			   ind + len otherwise.
 */
array_ui array_ubound(struct array *array, const void *data, 
	 array_ui ind, array_ui len, int (*comp_fn)(const void*, const void*));

/**
 * array_bsearch
 *
 * @brief: binary-search within a range
 *
 * @array: pointer to array
 * @data: pointer to item to be searched
 * @ind: index at which to search
 * @len: number of items
 * @comp_fn: compare function
 *
 * NOTE: THE SEARCH RANGE MUST BE SORTED!
 *
 * array_bsearch searches for an item that is equal to data at @data among
 * @len items at index @ind
 * 
 * @comp_fn should returns an integer < 0, == 0, > 0 if the left item is
 * less than, equal to, or greater than the right item, respectively.
 *
 * returns index of an item if found, ind + len otherwise.
 */
array_ui array_bsearch(struct array *array, const void *data, 
		array_ui ind, array_ui len, int (*comp_fn)(const void*, const void*));

/**
 * array_lsearch
 *
 * @brief: linearly search within a range
 *
 * @array: pointer to array
 * @data: pointer to item to be searched
 * @ind: index at which to search
 * @len: number of items
 * @comp_fn: compare function
 *
 * array_lsearch searches for an item that is equal to data at address 
 * @data among @len items at index @ind
 * 
 * @comp_fn should returns an integer < 0, == 0, > 0 if the left item is
 * less than, equal to, or greater than the right item, respectively.
 *
 * returns index of an item if found, ind + len otherwise.
 */
array_ui array_lsearch(struct array *array, const void *data, 
		array_ui ind, array_ui len, int (*comp_fn)(const void*, const void*));

/**
 * array_find
 *
 * @brief: finds the first item within a range
 *
 * @array: pointer to array
 * @ind: index at which to find
 * @len: range
 * @pred_fn: predicate function
 *
 * array_find linearly find the first item that satisfies @pred_fn, that is,
 * if @pred_fn returns non-zero, from a number of @len items at index @ind.
 *
 * returns index of the item if found, ind + len otherwise.
 */
array_ui array_find(struct array *array, array_ui ind, 
		array_ui len, int (*pred_fn)(const void*));

/**
 * array_rfind
 *
 * @brief: find in reverse order
 *
 * @array: pointer to array
 * @ind: index at which to find
 * @len: range
 * @pred_fn: predicate function
 *
 * returns index of the item if found, ind + len otherwise.
 */
array_ui array_rfind(struct array *array, array_ui ind, 
		array_ui len, int (*pred_fn)(const void*));

/**
 * array_write
 *
 * @brief: overwrites items with new items at a specified index
 *
 * @array: pointer to array
 * @data: items to be written
 * @ind: index at which to write
 * @len: number of items to be overwritten
 *
 * array_write writes a number of @len items at index @ind with @len items
 * from @data, if @data is NULL, the internal items with be set to 0
 *
 * returns nothing.
 */
void array_write(struct array *array, const void *data, 
		array_ui ind, array_ui len);

/**
 * array_rwrite
 *
 * @brief: write data repeatedly
 *
 * @array: pointer to array
 * @data: item to be written
 * @ind: index at which to write
 * @len: number of items to be repeated
 *
 * array_rwrite writes @data a @len number of times at index @ind
 * 0 is written if @data is NULL
 *
 * returns nothing.
 */
void array_rwrite(struct array *array, const void *data, 
		array_ui ind, array_ui len);

/**
 * array_sorted
 *
 * @brief: check if items are sorted
 *
 * @array: pointer to array
 * @ind: index at which to check
 * @len: range
 * @comp_fn: compare function
 *
 * check if a number of @len items are sorted at index @ind
 *
 * returns 1 if sorted, 0 otherwise.
 */
int array_sorted(struct array *array, array_ui ind, array_ui len,
		int (*comp_fn)(const void*, const void*));

/**
 * array_clear
 *
 * @brief: frees the internal block
 *
 * @array: pointer to array
 *
 * free everything of an array (except the pointer to array)
 *
 * returns nothing.
 */
void array_clear(struct array *array);

/**
 * array_free
 *
 * @brief: frees the internal block and array
 *
 * @array: pointer to array
 *
 * @array must be dynamically allocated in order to be array_free'd.
 *
 * returns nothing.
 */
void array_free(struct array *array);

/**
 * array_print
 *
 * @brief: print all items
 *
 * @array: pointer to array 
 * @ind: index at which to print
 * @len: range
 * @delimiter: delimiter between each item's print, defaulted to \n if NULL
 * @display: format function where each item is printed
 *
 * this function should only be used if built with 'build_print' or 
 * 'buildtype' of type debug
 *
 * override buildtype with debug or debugoptimized to enable this function
 * or comment them out if you want it available in the release build
 *
 * returns nothing.
 */
void array_print(struct array *array, array_ui ind, 
		 array_ui len, const char *delimiter, void (*display)(const void *));

/**
 * array_append
 *
 * @brief: pushes a new item at the array's end
 *
 * @array: pointer to array
 * @data: pointer to item to be pushed
 *
 * returns a pointer to newly pushed element if successful, 
 *         NULL if no memory.
 */
ARRAY_INLINE void* array_append(struct array *array, const void *data);

/**
 * array_push
 *
 * @brief: pushes a new item at the array's end without checking
 *
 * @array: pointer to array
 *
 * array_push increments @array->len and returns the new address.
 * NOTE: no more space is allocated when the array is full, so do user
 * array_full() before pushing.
 *
 * returns a pointer to newly pushed element if successful, 
 *         NULL if no memory.
 */
ARRAY_INLINE void* array_push(struct array *array);

/**
 * array_pop
 * 
 * @brief: removes the item at the end
 *
 * @array: pointer to array
 *
 * pops the end item by returning its address and decrements @array->len.
 * NOTE: like array_pop(), bound is not checked so use array_empty() before
 * popping.
 *
 * returns address of the newly popped element.
 */
ARRAY_INLINE void* array_pop(struct array *array);

/**
 * array_reserve
 * 
 * @brief: reserve a zero'd out block for later use
 *
 * @array: pointer to array
 * @len: number of slot
 *
 * returns a pointer to newly reserved slots if successful, NULL otherwise.
 */
ARRAY_INLINE void* array_reserve(struct array *array, array_ui len);

/**
 * array_dup
 *
 * @brief: duplicates an array
 *
 * @array: pointer to array
 * @copy_fn: custom copy function
 *
 * array_dup fully duplicates an array, memcpy will be used if copy_fn is
 * NULL.
 *
 * returns a newly duplicated array if successful, NULL otherwise.
 */
ARRAY_INLINE struct array* array_dup(struct array *array, 
		void (*copy_fn)(void*, const void*));

/**
 * array_shrink
 *
 * @brief: shrinks unused slots
 *
 * @array: pointer to array
 *
 * array_shrink resizes @array to cap = len
 *
 * returns @array if successful, NULL otherwise.
 */
ARRAY_INLINE void* array_shrink(struct array *array);

/**
 * array_trunc
 *
 * @brief: truncates an array length to 0
 *
 * @array: pointer to array
 *
 * returns nothing.
 */
ARRAY_INLINE void array_trunc(struct array *array);

/**
 * array_erase
 *
 * @brief: erase all items at an index
 *
 * @array: pointer to array
 * @ind: index at which to erase
 *
 * erase all items at @ind to the end
 * resulting in @array->len = ind
 *
 * returns nothing.
 */
ARRAY_INLINE void array_erase(struct array *array, array_ui ind);

/**
 * array_lrotate
 *
 * @brief: rotate left
 *
 * @array: pointer to array
 * @ind: index at which to rotate
 * @len: rotated range
 * @offset: rotate how many items
 *
 * returns nothing.
 */
ARRAY_INLINE void array_lrotate(struct array *array, array_ui ind, 
		array_ui len, array_ui offset);

/**
 * array_rrotate
 *
 * @brief: rotate right
 *
 * @array: pointer to array
 * @ind: index at which to rotate
 * @len: rotated range
 * @offset: rotate how many items
 *
 * returns nothing.
 */
ARRAY_INLINE void array_rrotate(struct array *array, array_ui ind, 
		array_ui len, array_ui offset);

/**
 * array_get
 *
 * @brief: gets an item's address
 *
 * @array: pointer to array
 * @ind: item's index
 *
 * returns the item's address.
 */
ARRAY_INLINE void* array_get(struct array *array, array_ui ind);

/**
 * array_index
 *
 * @brief: gets an item's index
 *
 * @array: pointer to array
 * @datap: item's address
 *
 * datap must be the valid address of an item from @array
 *
 * returns the item's index.
 */
ARRAY_INLINE array_ui array_index(struct array *array, const void *datap);

/**
 * array_vacancy
 *
 * @brief: gets number of unused slots
 *
 * @array: pointer to array
 *
 * equivalent to cap - len
 *
 * returns number of unused slots.
 */
ARRAY_INLINE array_ui array_vacancy(struct array *array);

/**
 * array_data
 *
 * @brief: gets the internal data block
 *
 * @array: pointer to array
 *
 * returns @array's internal data block.
 */
ARRAY_INLINE void* array_data(struct array *array);

/**
 * array_size
 *
 * @brief: gets item size
 *
 * @array: pointer to array
 *
 * returns @array's item size.
 */
ARRAY_INLINE size_t array_size(struct array *array);

/**
 * array_len
 *
 * @brief: gets number of items
 *
 * @array: pointer to array
 *
 * returns @array's length.
 */
ARRAY_INLINE array_ui array_len(struct array *array);

/**
 * array_cap
 *
 * @brief: gets capacity
 *
 * @array: pointer to array
 *
 * returns @array's capacity.
 */
ARRAY_INLINE array_ui array_cap(struct array *array);

/**
 * array_end
 *
 * @brief: gets length from a specified index to the end
 *
 * @array: pointer to array
 * @ind: index at which to compute the length
 *
 * array_end computes the length beginning at index @ind to @array's end
 *
 * returns computed length.
 */
ARRAY_INLINE array_ui array_end(struct array *array, array_ui ind);

/**
 * array_rind
 * @brief: gets reverse index
 *
 * @array: pointer to array
 * @rind: reverse index
 *
 * array_rind computes the actual index from reverse index @rind
 * 
 * example: @rind = 0 is equivalent to index = @array->len - 1
 *
 * returns computed actual index.
 */
ARRAY_INLINE array_ui array_rind(struct array *array, array_ui rind);

/**
 * array_empty
 *
 * @brief: checks if array is empty
 *
 * @array: pointer to array
 *
 * returns 1 if empty, 0 otherwise.
 */
ARRAY_INLINE int array_empty(struct array *array);

/**
 * array_full
 *
 * @brief: checks if array is full
 *
 * @array: pointer to array
 *
 * check whether array's capacity is reached.
 *
 * returns 1 if full, 0 otherwise.
 */
ARRAY_INLINE int array_full(struct array *array);

void* 
array_append(struct array *array, const void *data)
{
	return array_insert(array, data, array->len, 1);
}

void* 
array_push(struct array *array)
{
	return array_get(array, array->len++);
}

void* 
array_pop(struct array *array)
{
	return array_get(array, --array->len);
}

void*
array_reserve(struct array *array, array_ui len)
{
	return array_insert(array, NULL, array->len, len);
}

struct array* 
array_dup(struct array *array, 
		void (*copy_fn)(void*, const void*))
{
	return array_copy(array, 0, array->len, copy_fn);
}

void* 
array_shrink(struct array *array)
{
	return array_resize(array, array->len);
}

void 
array_trunc(struct array *array)
{
	array->len = 0;
}

void
array_erase(struct array *array, array_ui ind)
{
	array->len = ind;
}

void
array_lrotate(struct array *array, array_ui ind, array_ui len, 
		array_ui offset) { 
	array_rotate(array, ind, len, -(array_sl)offset);
}

void
array_rrotate(struct array *array, array_ui ind, array_ui len, 
		array_ui offset) { 
	array_rotate(array, ind, len, offset);
}

void* 
array_get(struct array *array, array_ui ind)
{
	return (array_uc*)array->data + ind * array->size;
}

array_ui 
array_index(struct array *array, const void *datap)
{
	return ((const array_uc*)datap - (array_uc*)array->data) / 
		array->size;
}

array_ui 
array_vacancy(struct array *array)
{
	return array->cap - array->len;
}

void* 
array_data(struct array *array)
{
	return array->data;
}

size_t 
array_size(struct array *array)
{
	return array->size;
}

array_ui 
array_len(struct array *array)
{
	return array->len;
}

array_ui 
array_cap(struct array *array)
{
	return array->cap;
}

array_ui 
array_end(struct array *array, array_ui ind)
{
	return array->len - ind;
}

array_ui 
array_rind(struct array *array, array_ui rind)
{
	return array->len - 1 - rind;
}

int 
array_empty(struct array *array)
{
	return array->len == 0;
}

int
array_full(struct array *array)
{
	return array->len == array->cap;
}

#endif /* _ARRAY_H */
