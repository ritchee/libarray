option('build_tests', type: 'boolean', value: false, description: 'build unit tests')
option('build_print', type: 'boolean', value: false, description: 'enable ARRAY_PRINT')
option('no_panic', type: 'boolean', value: false, description: 'panic if errno is ENOMEM')
