# libarray
Minimal dynamic array implementation

## Usage
The files can be modified and integrated into your project.
Otherwise, go to Installation to build a library.

~~There's also a libarray-safe branch that includes error checkings.~~
Branch libarray-safe is abandoned, you can implement it yourself or simply 
perform safety checks before calling an array routine.

Use branch posix for POSIX systems.
```bash
git checkout posix
```

## Installation
```bash
# build
meson "build"

# build a static library
meson "build" -Ddefault_library=static

# build with a custom prefix, replace /path/of/prefix with yours
meson "build" --prefix=/path/of/prefix

# compile
cd build
ninja

# install
ninja install

#uninstall
ninja uninstall
```

## Documentation
Check `array.h`

## Version
1.19.0

## License
MIT
