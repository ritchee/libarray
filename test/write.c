#include "test.h"

int main(void)
{
	struct array *ar;
	int data1[]    = {23, 2, 11, 33, 39, 32, 34, 12, 10, 9};
	int data2[]    = {0, 1, 0};
	int expected[] = {23, 2, 0, 1, 0, 32, 34, 12, 10, 9};

	ar = new_array(data1, DATA_LEN(data1));

	array_write(ar, data2, 2, DATA_LEN(data2));
	ASSERT_EQ_MEM(ar->data, expected, ar->len);

	PASS();

	return 0;
}
