#include "test.h"

int main(void)
{
	struct array ar;

	array_init(&ar, sizeof(int), 10);

	ASSERT_EQ_NUM(ar.len, 0);
	ASSERT_EQ_NUM(ar.cap, 10);
	ASSERT_EQ_NUM(ar.size, sizeof(int));

	PASS();

	return 0;
}
