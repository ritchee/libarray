#include "test.h"

int main(void)
{
	struct array ar;
	int data[]      = {23, 2, 11, 33, 39, 32, 34, 12, 10, 9};
	int expected1[] = {23, 2, 11, 12, 10, 9, 33, 39, 32, 34};
	int expected2[] = {23, 2, 11, 12, 10, 9, 33, 39, 32, 34, 23, 2};

	array_init(&ar, sizeof(int), 10);
	array_insert(&ar, data, 0, 7);

	ASSERT_EQ_MEM(ar.data, data, 7);
	array_insert(&ar, &data[7], 3, DATA_LEN(data) - 7);
	ASSERT_EQ_MEM(ar.data, expected1, ar.len);
	array_insert(&ar, data, ar.len, 2);
	ASSERT_EQ_MEM(ar.data, expected2, ar.len);
	ASSERT_EQ_NUM(ar.cap, 20);
	
	PASS();

	return 0;
}
