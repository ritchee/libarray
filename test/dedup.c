#include "test.h"

int main(void)
{
	struct array *ar;
	int data[]     = {9, 2, 11, 33, 39, 33, 34, 2, 10, 9};
	int expected[] = {9, 2, 11, 33, 39, 34, 10, 9};

	ar = new_array(data, DATA_LEN(data));

	array_dedup(ar, 1, 7, compar);
	ASSERT_EQ_MEM(ar->data, expected, ar->len);

	PASS();

	return 0;
}
