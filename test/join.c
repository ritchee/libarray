#include "test.h"

int main(void)
{
	struct array *ar, *join;
	int data[]      = {23, 2, 11};
  int join_data[] = {33, 39, 32, 34, 12, 10, 9};
  int expected[]  = {23, 2, 33, 39, 32, 34, 12, 10, 9, 11};

	ar = new_array(data, DATA_LEN(data));
	join = new_array(join_data, DATA_LEN(join_data));

	array_join(ar, ar->len - 1, join);

	ASSERT_EQ_MEM(ar->data, expected, ar->len);
		
	PASS();

	return 0;
}
