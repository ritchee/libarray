#include "test.h"

int main(void)
{
	struct array *ar;
	int data[] = {2, 9, 10, 11, 13, 23, 32, 33, 34, 39};
	array_ui ret;
	int n;

	ar = new_array(data, DATA_LEN(data));

	n = 13;
	ret = array_lbound(ar, &n, 1, 7, compar);
	ASSERT_EQ_NUM(ret, 4);
	n = 1;
	ret = array_lbound(ar, &n, 1, 7, compar);
	ASSERT_EQ_NUM(ret, 1);
	n = 51;
	ret = array_lbound(ar, &n, 1, 7, compar);
	ASSERT_EQ_NUM(ret, 1 + 7);

	PASS();

	return 0;
}
