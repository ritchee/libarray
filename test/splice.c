#include "test.h"

int main(void)
{
	struct array *ar, *splice;
	int data[]      = {23, 2, 11, 33, 39, 32, 34, 12, 10, 9};
	int expected1[] = {23, 2, 32, 34, 12, 10, 9};
	int expected2[] = {11, 33, 39};

	ar = new_array(data, DATA_LEN(data));
	
	splice = array_splice(ar, 2, 3, NULL);

	ASSERT_EQ_MEM(ar->data, expected1, ar->len);
	ASSERT_EQ_MEM(splice->data, expected2, splice->len);

	PASS();

	return 0;
}
