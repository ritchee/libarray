#include "test.h"

int main(void)
{
	struct array *ar;
	int data[] = {23, 2, 11, 33, 39, 32, 34, 12, 10, 9};

	ar = new_array(data, DATA_LEN(data));

	array_shuffle(ar, 2, 6);
	ASSERT_NEQ_MEM(array_get(ar, 2), &data[2], 5);

	PASS();

	return 0;
}
