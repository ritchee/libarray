
#include "test.h"

int main(void)
{
	struct array *ar;
	int data[] = {23, 2, 11, 33, 39, 32, 34, 12, 10, 9};
	array_ui ret;
	int n;

	ar = new_array(data, DATA_LEN(data));

	n = 39;
	ret = array_lsearch(ar, &n, 1, 7, compar);
	ASSERT_EQ_NUM(ret, 4);
	n = 51;
	ret = array_lsearch(ar, &n, 1, 7, compar);
	ASSERT_EQ_NUM(ret, 1 + 7);

	PASS();

	return 0;
}
