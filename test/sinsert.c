#include "test.h"

int main(void)
{
	struct array ar;
	int i;
	int data[]      = {23, 2, 11, 33, 39, 32, 34, 12, 10, 9};
	int expected1[] = {2, 9, 10, 11, 12, 23, 32, 33, 34, 39};
	int expected2[] = {2, 3, 9, 10, 11, 12, 16, 23, 32, 33, 34, 39};

	array_init(&ar, sizeof(int), 10);

	for (i = 0; i < (int)DATA_LEN(data); i++)
		array_sinsert(&ar, &data[i], 0, ar.len, compar);

	ASSERT_EQ_MEM(ar.data, expected1, ar.len);
	i = 3;
	array_sinsert(&ar, &i, 1, 5, compar);
	i = 16;
	array_sinsert(&ar, &i, 1, 5, compar);
	ASSERT_EQ_MEM(ar.data, expected2, ar.len);

	PASS();

	return 0;
}
