#ifndef _TEST_H
#define _TEST_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "../array.h"

#define DATA_LEN(a) (sizeof(a) / sizeof(a[0]))

const char *name = TEST_NAME;

struct array *
new_array(int *data, size_t len)
{
	return array_new(data, sizeof(int), len);
}

int 
compar(const void *p1, const void *p2)
{
	int i1, i2;

	i1 = *(const int *)p1;
	i2 = *(const int *)p2;

	return (i1 < i2) ? -1 : (i1 > i2) ? 1 : 0;
}

int
is_odd(const void *p)
{
	return *(const int *)p % 2;
}

void 
print(struct array *ar)
{
	ARRAY_FOR(ar, i) {
		printf("%d ", ARRAY_DEREF(ar, i, int));
	}
	puts("");
}

int (*assert_compar)(const void *, const void *) = compar;

#define PASS() pass("passed")

void 
pass(const char *msg)
{
	extern const char *name;

	printf("PASS: %s %s\n", name, msg);
}

void 
fail(const char *msg)
{
	extern const char *name;

	printf("FAIL: %s %s\n", name, msg);
}

#define ASSERT_EQ_NUM(rn, cn) assert_eq_num((size_t)(rn), (size_t)(cn))
#define ASSERT_EQ_MEM(rn, cn, len) \
	assert_eq_mem((const void *)(rn), (const void *)(cn), (size_t)(len))
#define ASSERT_NEQ_MEM(rn, cn, len) \
	assert_neq_mem((const void *)(rn), (const void *)(cn), (size_t)(len))

void 
assert_eq_num(size_t rn, size_t cn)
{
	char buf[4096];
	
	if (rn != cn) {
		snprintf(buf, sizeof(buf), "expected %ld, got %ld", cn, rn);
		fail(buf);

		exit(1);
	}
}

void
assert_eq_mem(const void *p1, const void *p2, size_t len)
{
	const int *ip1, *ip2, *end1, *end2;

	ip1 = p1;
	ip2 = p2;
	end1 = &ip1[len];
	end2 = &ip2[len];

	while (ip1 < end1) {
		if (assert_compar(ip1, ip2) != 0) {
			fail("incorrect order");
			printf("\tgot:      ");
			for (ip1 = p1; ip1 < end1; ip1++)
				printf("%d ", *ip1);
			puts("");

			printf("\texpected: ");
			for (ip2 = p2; ip2 < end2; ip2++)
				printf("%d ", *ip2);
			puts("");

			exit(1);
		}

		ip1++;
		ip2++;
	}
}

void 
assert_neq_mem(const void *p1, const void *p2, size_t len)
{
	if (memcmp(p1, p2, sizeof(int) * len) == 0) {
		fail("shuffling failed");
		exit(1);
	}
}

#endif  /* _TEST_H */

