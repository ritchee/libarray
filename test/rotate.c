#include "test.h"

int main(void)
{
	struct array *ar;
	int data[]       = {23, 2, 11, 33, 39, 32, 34, 12, 10, 9};
	int expected1[]  = {23, 2, 32, 34, 11, 33, 39, 12, 10, 9};
	int expected2[]  = {2, 32, 34, 11, 33, 39, 12, 10, 9, 23};

	ar = new_array(data, DATA_LEN(data));

	array_rotate(ar, 2, 5, -3);
	ASSERT_EQ_MEM(ar->data, expected1, ar->len);
	array_rotate(ar, 0, ar->len, -11);
	ASSERT_EQ_MEM(ar->data, expected2, ar->len);

	PASS();

	return 0;
}
