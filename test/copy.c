#include "test.h"

int main(void)
{
	struct array *ar, *cpy;
	int data[]     = {23, 2, 11, 33, 39, 32, 34, 12, 10, 9};
	int expected[] = {33, 39, 32, 34};

	ar = new_array(data, DATA_LEN(data));

	cpy = array_copy(ar, 3, 4, NULL);

	ASSERT_EQ_MEM(cpy->data, expected, 4);
		
	PASS();

	return 0;
}
