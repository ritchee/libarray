#include "test.h"

int main(void)
{
	struct array *ar;
	int data1[] = {23, 2, 11, 33, 39, 32, 34, 12, 10, 9};
	int data2[] = {2, 9, 10, 11, 13, 23, 32, 33, 34, 39};
	int is_sorted;

	ar = new_array(data1, DATA_LEN(data1));

	is_sorted = array_sorted(ar, 0, ar->len, compar);
	ASSERT_EQ_NUM(is_sorted, 0);

	array_free(ar);

	ar = new_array(data2, DATA_LEN(data2));
	is_sorted = array_sorted(ar, 0, ar->len, compar);
	ASSERT_EQ_NUM(is_sorted, 1);
	
	PASS();

	return 0;
}
