#include "test.h"

int main(void)
{
	struct array *ar;
	int data[] = {23, 2, 11, 33, 39, 32, 34, 12, 10, 9};

	ar = new_array(data, DATA_LEN(data));

	array_resize(ar, 13);
	ASSERT_EQ_NUM(ar->cap, 13);

	PASS();

	return 0;
}
