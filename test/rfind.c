#include "test.h"

int main(void)
{
	struct array *ar;
	int data[] = {23, 2, 11, 33, 39, 32, 34, 12, 10, 9};
	array_ui ret;

	ar = new_array(data, DATA_LEN(data));

	ret = array_rfind(ar, 1, 7, is_odd);
	ASSERT_EQ_NUM(ret, 4);
	ret = array_rfind(ar, 7, 2, is_odd);
	ASSERT_EQ_NUM(ret, 7 + 2);

	PASS();

	return 0;
}
