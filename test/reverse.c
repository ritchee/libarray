#include "test.h"

int main(void)
{
	struct array *ar;
	int data[]     = {23, 2, 11, 33, 39, 32, 34, 12, 10, 9};
	int expected[] = {23, 2, 12, 34, 32, 39, 33, 11, 10, 9};

	ar = new_array(data, DATA_LEN(data));

	array_reverse(ar, 2, 6);
	ASSERT_EQ_MEM(ar->data, expected, ar->len);

	PASS();

	return 0;
}
