#include "test.h"

int main(void)
{
	struct array *ar;
	int data[]     = {23, 2, 11, 33, 39, 32, 34, 12, 10, 9};
	int expected[] = {23, 2, 12, 10, 9, 11, 33, 39, 32, 34};
	array_ui ind;

	ar = new_array(data, DATA_LEN(data));

	ind = array_remove(ar, 2, 5);
	ASSERT_EQ_MEM(ar->data, expected, ar->len);
	ASSERT_EQ_NUM(ind, 5);

	PASS();

	return 0;
}
