#include "test.h"

int main(void)
{
	struct array *ar;
	int data[] = {23, 2, 11, 33, 39, 32, 34, 12, 10, 9};

	ar = new_array(data, DATA_LEN(data));

	array_extend(ar, 7);
	ASSERT_EQ_NUM(ar->cap, DATA_LEN(data) + 7);

	PASS();

	return 0;
}
