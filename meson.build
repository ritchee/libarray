project('libarray', 'c',
  version: '1.19.0',
  license: 'MIT',
  default_options: [
    'warning_level=3', 
    'buildtype=release',
    'c_std=c99',
    ])

src = ['array.c']
prefix = get_option('prefix')
install_perm = 'rw-r--r--'

if get_option('default_library') == 'shared'
  add_project_arguments('-DARRAY_SHARED', '-DARRAY_EXPORT', language: 'c')
endif

build_type = get_option('buildtype')
if build_type.contains('debug') or get_option('build_print') == true
  add_project_arguments('-DARRAY_DEBUG', language: 'c')
endif

if get_option('no_panic') == true
  add_project_arguments('-DARRAY_NPANIC', language: 'c')
endif

libarray = library(
  'array', 
  src,
  install: true,
  install_dir: prefix + '/lib',
  version: meson.project_version()
  )

install_headers(
  'array.h', 
  install_mode: install_perm, 
  install_dir: prefix + '/include'
  )

executable('example', 'example.c', link_with: libarray)

if get_option('build_tests') == true
  subdir('test')
endif
