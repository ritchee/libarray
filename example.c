#include <stdio.h>
#include <stdarg.h>
#include <assert.h>

#include "array.h"

#define err(...) do { \
	fprintf(stderr, __VA_ARGS__); \
	exit(1); \
} while (0)

int int_compar(const void* lp, const void* rp)
{
	int n1, n2;

	n1 = *(const int*)lp;
	n2 = *(const int*)rp;

	if (n1 < n2)
		return -1;
	if (n1 > n2)
		return 1;
	return 0;
}

int odd(const void* p)
{
	int n;

	n = *(const int*)p;

	return n % 2;
}

void print(struct array* ar, const char* op, ...)
{
	va_list args;

	va_start(args, op);
	vfprintf(stdout, op, args);
	va_end(args);
	puts("");

	printf("length: %u\tcapacity: %u\n", ar->len, ar->cap);
	ARRAY_FOR(ar, i) {
		printf("%d ", ARRAY_DEREF(ar, i, int));
	}
	puts("\n");
}

int main(void)
{
	int i, n;
	array_ui ind;
	struct array ar, sar;

	/* init array with 20 int elements */
	n = 20;
	if (!array_init(&ar, sizeof(int), n))
		err("failed to init\n");

	for (i = 0; i < n; i++) {
		if (!array_append(&ar, &i))
			err("failed to push %d\n", i);
	}
	print(&ar, "appending %d ints", n);

	/* shuffle all ints */
	array_shuffle(&ar,
			0,     /* start at 0  */
			ar.len /* shuffle all */
			);
	print(&ar, "shuffle all ints");

	/* stably partition odd ints to the end */
	ind = array_spartition(&ar,
			0,      /* start at 0                                          */
			ar.len, /* partition all                                       */
			odd     /* predicate function                                  */
			);
	print(&ar, "stable partition odd ints to %u", ind);

	/* sort each partition */
	array_sort(&ar, 
			0,         /* start at 0               */
			ind,       /* sort all ints before ind */
			int_compar /* compare function         */
			);
	/* array_end(&ar, ind) == ar.len - ind */
	array_sort(&ar, 
			ind,                 /* start at ind              */
			array_end(&ar, ind), /* sort all beginning at ind */
			int_compar           /* compare function          */
			);
	print(&ar, "sort 2 partitions");

	/* splicing into a new array of odd */
	/* make sure destination array is initialized and has enough capacity */
	if (array_init(&sar, sizeof(int), ar.len - ind) == NULL)
		err("failed to init a new array");
	array_splice(&ar, 
			ind,          /* start at ind        */
			ar.len - ind, /* splice all odd ints */
			&sar          /* destination array   */
			);
	if (sar.len == 0)
		err("failed to splice\n");
	puts("splicing odd partition into a new array");
	print(&ar, "even partition");
	print(&sar, "odd partition");

	/* rejoin at half of ar.len */
	array_join(&ar, 
			ar.len / 2, /* join at half length      */
			&sar        /* pointer to array to join */
			);
	print(&ar, "join 2 arrays");

	/* reserve 3 slots == insert 3 zeros at the end */
	if (!array_reserve(&ar, 3))
		err("failed to reserve");
	print(&ar, "reserve 3 slots at the end");

	array_shuffle(&ar, 0, ar.len);
	print(&ar, "shuffle all ints");

	/* remove duplicates */
	array_dedup(&ar, 
			0,         /* start at 0                     */
			ar.len,    /* remove all duplicates possible */
			int_compar /* compare function               */
			);
	print(&ar, "remove duplicates, namely the 0s");

	/* extend more slots */
	printf("cap before extension: %u\n", ar.cap);
	if (!array_extend(&ar, n))
		err("failed to extend\n");
	printf("cap after extending %d: %u\n", n, ar.cap);

	/* quick-push 5 ints, use QPUSH with caution */
	n = i + 5;
	while (!array_full(&ar) && i < n)
		ARRAY_QPUSH(&ar, int) = i++;
	print(&ar, "quick-push 5 more ints");

	/* delete first 3 ints */
	array_delete(&ar, 
			0, /* start at 0        */
			3  /* delete 3 elements */
			);
	print(&ar, "delete first 3 ints");

	/* reverse */
	array_reverse(&ar, 
			0,     /* start at 0           */
			ar.len /* reverse all elements */
			);
	print(&ar, "reverse the entire array");

	/* binary search */
	i = ARRAY_DEREF(&ar, ar.len / 2, int);
	array_sort(&ar, 0, ar.len, int_compar);
	print(&ar, "sort the entire array");
	ind = array_bsearch(&ar, 
			&i,        /* pointer to search data */
			0,         /* start 0                */
			ar.len,    /* search all elements    */
			int_compar /* compare function       */
			);
	printf("bsearch found %d at %u\n", i, ind);

	/* interpose everything after ind to the beginning */
	/* ind is 0, so change it for the sake of demo */
	if (ind == 0)
		ind = ar.len / 2;
	array_interpose(&ar, 
			ind,                  /* start of the block             */
			2,                    /* number of elements to be moved */
			0 - (array_sl)ind / 2 /* move to the beginning          */
			);
	print(&ar, "interpose 2 ints from %u back %u elements ", ind, ind / 2);

	/* rotate 5 elements to the right */
	array_rotate(&ar, 
			0,      /* start of the block        */
			ar.len, /* rotate all elements       */
			5       /* rotate forward 5 elements */
			);
	print(&ar, "rotate the entire array 5 elements forward");

	printf("printing content with macro:\n");
	ARRAY_ITER(&ar, iter, int) {
		printf("%d ", *iter);
	}
	puts("");

	/* clear arrays */
	array_clear(&sar);
	array_clear(&ar);

	return 0;
}

