#ifdef _MSC_VER
#define _CRT_RAND_S
#endif /* _MSC_VER */

#include <errno.h> /* EOVERFLOW */
#include <assert.h>

#include "array.h"

#define THRESHOLD 16
#define CACHE_SIZE 4096

#ifdef ARRAY_NPANIC
ARRAY_INLINE
void *
la_alloc(size_t size)
{
	void *mptr;
	
	mptr = ARRAY_ALLOC(size);
	assert(mptr);

	return mptr;
}

ARRAY_INLINE
void * 
la_realloc(void *ptr, size_t size)
{
	void *mptr;
	
	mptr = ARRAY_REALLOC(ptr,size);
	assert(mptr);

	return mptr;
}

#undef ARRAY_ALLOC
#define ARRAY_ALLOC la_alloc

#undef ARRAY_REALLOC
#define ARRAY_REALLOC la_realloc

#endif /* ARRAY_NPANIC */

static array_ui la_seed[4] = {
	1777456429,
	3007012567,
	4219636203,
	143377625
};

ARRAY_INLINE array_ui la_rotl(array_ui x, int k);
ARRAY_INLINE array_ui la_rand(void);
ARRAY_INLINE array_ui la_rand_range(array_ui h);

array_ui
la_rotl(array_ui x, int k)
{
	return (x << k) | (x >> (-k & 31));
}

array_ui
la_rand(void)
{
	array_ui *s = la_seed;
	array_ui r = la_rotl(s[1] * 5, 7) * 9;
	array_ui t = s[1] << 9;

	s[2] ^= s[0];
	s[3] ^= s[1];
	s[1] ^= s[2];
	s[0] ^= s[3];

	s[2] ^= t;

	s[3] = la_rotl(s[3], 11);

	return r;
}

array_ui
la_rand_range(array_ui h)
{
	array_ui t, x;

	// if h is a power of 2
	if ((h & (h - 1)) == 0)
		return la_rand() & (h - 1);

	// take top rejection range instead of bottom
	t = (array_ui)-1 - (-h % h);

	do {
		x = la_rand();
	} while (x > t);

	return x % h;
}

static void a_mswap(void *restrict p1, void *restrict p2, size_t size);

void 
a_mswap(void *restrict p1, void *restrict p2, size_t size)
{
	array_ul *_lp1, *_lp2, ltmp;
	array_uc *_p1, *_p2, tmp;

	_lp1 = p1;
	_lp2 = p2;

	while (size >= sizeof(array_ul)) {
		ltmp = *_lp1;
		*_lp1++ = *_lp2;
		*_lp2++ = ltmp;
		size -= sizeof(array_ul);
	}

	_p1 = (array_uc *)_lp1;
	_p2 = (array_uc *)_lp2;

	while (size--) {
		tmp = *_p1;
		*_p1++ = *_p2;
		*_p2++ = tmp;
	}
}

struct array* 
array_new(const void *data, size_t size, array_ui len)
{
	struct array *ar;

	ar = ARRAY_ALLOC(sizeof(struct array));
	if (ar == NULL)
		return NULL;

	if (array_init(ar, size, len) == NULL) {
		ARRAY_FREE(ar);

		return NULL;
	}

	if (data != NULL) {
		memcpy(ar->data, data, len * size);
		ar->len = len;
	}

	return ar;
}

struct array* 
array_init(struct array *array, size_t size, array_ui cap)
{
	if (size == 0 || cap == 0)
		return NULL;

	if ((array->data = ARRAY_ALLOC(size * cap)) == NULL)
		return NULL;

	array->size = size;
	array->len = 0;
	array->cap = cap;

	return array;
}

void* 
array_insert(struct array *array, const void *data, 
		array_ui ind, array_ui dlen)
{
	size_t move_size;
	array_uc *it;
	array_ui new_cap, new_len;

	new_len = dlen + array->len;

	/* handle overflow */
	if (new_len < dlen) {
		errno = EOVERFLOW;

		return NULL;
	}
	/* extend if new len is greater than array can hold */
	else if (new_len > array->cap) {
		/* double the current capacity */
		new_cap = array->cap * 2;
		/* return NULL if new cap or len overflowed */
		if (new_cap < array->cap) {
			errno = EOVERFLOW;

			return NULL;
		}
		else if (new_cap < new_len) {
			/* new cap is not enough, so extend to new len */
			new_cap = new_len;
		}

		/* resize the old block */
		if (array_resize(array, new_cap) == NULL)
			return NULL;
	}

	/* move the block at the requested index to reserve space for new data */
	it = array_get(array, ind);
	move_size = (array->len - ind) * array->size;
	if (move_size != 0)
		memmove(it + dlen * array->size, it, move_size);

	/* zero out the newly inserted space */
	if (data == NULL)
		memset(it, 0, dlen * array->size);
	/* else copy into the internal block and update info */
	else 
		memcpy(it, data, dlen * array->size);
	array->len += dlen;
	
	return it;
}

void* array_sinsert(struct array *array, const void *data, 
		array_ui ind, array_ui len, int (*comp_fn)(const void*, const void*))
{
	return array_insert(
			array, 
			data, 
			array_ubound(array, data, ind, len, comp_fn),
			1
			);
}

void*
array_hpush(struct array *array, const void *data, array_ui ind,
		array_ui len, int (*comp_fn)(const void*, const void*))
{
	array_uc *it, *rit, *lit;
	array_ui tmp;

	if ((it = array_insert(array, data, ind + len, 1)) == NULL)
		return NULL;

	if (len == 0)
		return it;

	it = rit = array_get(array, ind);
	it -= array->size;
	len++;

	while (len > 1) {
		tmp = len / 2;
		rit = it + len * array->size;
		lit = it + tmp * array->size;
		if (comp_fn(lit, rit) >= 0) 
			break;

		a_mswap(lit, rit, array->size);
		len = tmp;
	}

	return rit;
}

void
array_hpop(struct array *array, void *rdata, array_ui ind, array_ui len,
		int (*comp_fn)(const void*, const void*))
{
	array_uc *it, *lit, *rit;
	array_ui tmp;

	it = array_get(array, ind);

	if (len == 0)
		return ;

	if (rdata)
		memcpy(rdata, it, array->size);

	rit = it + (--len) * array->size;
	memcpy(it, rit, array->size);
	array_delete(array, ind + len, 1);
	it -= array->size;
	len++;
	tmp = 1;

	while (tmp * 2 < len) {
		lit = it + tmp * array->size;
		tmp *= 2;
		rit = it + tmp * array->size;

		if (tmp + 1 < len && comp_fn(rit, rit + array->size) < 0) {
			tmp++;
			rit += array->size;
		}

		if (comp_fn(lit, rit) >= 0)
			break;

		a_mswap(lit, rit, array->size);
	}
}

void 
array_delete(struct array *array, array_ui ind, array_ui len)
{
	array_uc *it;
	size_t move_size;

	/**
	 * go to the indexed address 
	 * and calculate the size to move after deletion 
	 */
	it = array_get(array, ind);
	move_size = (array->len - (ind + len)) * array->size;

	/* overwrite the deleted block with the following block if existing */
	if (move_size != 0)
		memmove(it, it + len * array->size, move_size);

	/* decrease array's length */
	array->len -= len;
}

array_ui
array_remove(struct array *array, array_ui ind, array_ui len)
{
	array_ui range;

	range = array_end(array, ind);
	/* rotate the block that needs removing to the end of array */
	array_rotate(array, ind, range, range - len);

	return ind + (range - len);
}

void 
array_swap(struct array *array, array_ui ind1, array_ui ind2, array_ui len)
{
	if (len == 0)
		return ;

	if (ind1 != ind2)
		a_mswap(array_get(array, ind1), array_get(array, ind2), array->size * len);
}

struct array* 
array_copy(struct array *array, array_ui ind, array_ui len, 
		void (*copy_fn)(void*, const void*))
{
	struct array *ret;
	array_uc *it;

	if (len == 0 || 
			(ret = array_new(NULL, array->size, len)) == NULL)
		return NULL;

	/* go the index desired */
	it = array_get(array, ind);

	/* use memcpy unless supplied a custom copy function */
	if (copy_fn != NULL) {
		for (ind = 0; ind < len; ind++)
			copy_fn(array_get(ret, ind), it + ind * array->size);
	}
	else {
		memcpy(ret->data, it, len * array->size);
	}

	ret->len = len;

	return ret;
}

void* 
array_join(struct array *array, array_ui ind, struct array *to_join)
{
	void *ret;

	/* insert data from the requested array if it's not empty */
	ret = array_insert(array, to_join->data, ind, to_join->len);
	if (ret == NULL)
		return NULL;

	/* reset to join array's len since its data is joined*/
	to_join->len = 0;

	return ret;
}

array_ui
array_interpose(struct array *array, array_ui ind, 
		array_ui len, array_sl offset)
{
	array_sl tmp;

	/* return if there's nothing to interpose */
	if (len == 0 || offset == 0)
		return ind;

	/* move requested block according to the offset */
	if (offset < 0) {
		tmp = ind;
		tmp = -tmp;

		if (offset < tmp)
			offset = tmp;

		array_rotate(
				array, 
				(array_sl)ind + offset, 
				-offset + (array_sl)len, 
				offset); 
	}
	else {
		tmp = array->len - ind - len;

		if (offset > tmp)
			offset = tmp;

		array_rotate(
				array, 
				ind, 
				offset + len, 
				offset);
	}

	/* will return weird value if offset is not legit */
	return (array_sl)ind + offset;
}

void 
array_rotate(struct array *array, array_ui ind, array_ui len, array_sl offset)
{
	array_uc *it, *pivot, *cache;
	array_sl range, blk_len, tmp;
	size_t block_size, range_size;
	array_uc cache_block[CACHE_SIZE];

	it = array_get(array, ind);

	/* returns if there's nothing to do */
	if (len < 2 || offset % array->len == 0)
		return ;

	if (offset < 0) {
		/* get smallest valid offset to avoid moving data too much */
		offset = -offset;
		offset %= len;

		/* even smaller offset 
		 *
		 * needs blk_len to be as large as possible to reduce the amount of
		 * times data is moved around. And it also needs to be < len / 2, since
		 * the greater len will overlap in the following algorithm.
		 */
		if (offset > len / 2) {
			offset = blk_len = (array_sl)len - offset;
		}
		else {
			blk_len = offset;
			offset = -offset;
		}
	} 
	else {
		offset %= len;

		if (offset > len / 2) {
			offset = offset - (array_sl)len;
			blk_len = -offset;
		}
		else
			blk_len = offset;
	}

	range = (array_sl)len - blk_len;    /* length of the unaffected block */
	block_size = blk_len * array->size; /* size of the wrapped around block */
	range_size = range * array->size;   /* size of the unaffected block */

/* allocate a cache for faster rotation
	 * if cache cannot be allocated, move on to the in-place rotation
	 */
	if (block_size > sizeof(cache_block)) {
		cache = ARRAY_ALLOC(block_size);
		if (cache == NULL)
			goto rotate_in_place;
	} else {
		cache = cache_block;
	}

	if (offset > 0) { /* rotating right */
		pivot = it + range_size; /* go to where the wrapped around items begin */
		memcpy(cache, pivot, block_size); /* cache the wrapped around block */
		memmove(it + block_size, it, range_size); /* move the unaffected block */
		pivot = it; /* go to where the wrapped around block will go */
	}
	else { /* rotating left, same procedure */
		pivot = it;
		memcpy(cache, pivot, block_size);
		memmove(it, it + block_size, range_size);
		pivot = it + range_size;
	}

	memcpy(pivot, cache, block_size);

	if (block_size > sizeof(cache_block))
		ARRAY_FREE(cache);
	return ;

rotate_in_place:
	/* get range of rotated items, minus a block because pivot might be at the
	 * end of array since swapping data passed the end is obviously an error
	 */
	pivot = it + range_size;

	/* loop until there's only 1 item left, making offset == 0 */
	while (offset) {
		a_mswap(it, pivot, block_size);

		/* the remaining block has length less than blk_len */
		if ((tmp = range - blk_len) < blk_len) {
			/* reverse rotate direction for the remaining block */
			if (offset < 0) 
				offset = tmp;
			else {
				/* update iterator to the block to be rotated */
				it += block_size;
				offset = -tmp;
			}

			/* update new pivot, new range and block length */
			pivot = it + block_size;
			blk_len = tmp;
			range -= blk_len;
		}
		else {
			range -= blk_len;

			/* decrease pivot if rotating left, and vice versa */
			if (offset < 0)
				pivot -= block_size;
			else
				it += block_size;
		}
	}
}

struct array* 
array_splice(struct array *array, array_ui ind, array_ui len, 
		struct array *dest)
{
	array_uc *it;

	/* if a new array is requested, make a copy */
	if (dest == NULL) {
		dest = array_copy(array, ind, len, NULL);
		if (dest == NULL)
			return NULL;
	}
	else {
		/* go to the desired index*/
		it = array_get(array, ind);

		/* insert at the end */
		if (array_insert(dest, it, dest->len, len) == NULL)
			return NULL;
	}

	/* remove them from array */
	array_delete(array, ind, len);

	return dest;
}

void 
array_shuffle(struct array *array, array_ui ind, array_ui len)
{
	array_uc *it;
	array_ui rand_ind;

	/* go to the desired index */
	it = array_get(array, ind);

	for (ind = len - 1; ind > 0; ind--) {
		rand_ind = la_rand_range(ind + 1);

		if (rand_ind != ind) {
			a_mswap(it + array->size * ind, it + array->size * rand_ind,
					array->size);
		}
	}
}

void 
array_reverse(struct array *array, array_ui ind, array_ui len)
{
	array_uc *it, *rit;

	/* go to the desired indexes */
	it  = array_get(array, ind);
	rit = array_get(array, ind + len);
	rit -= array->size; /* prevent underflow */

	/* begin swapping elements until each element pointer crosses */
	while (it < rit) {
		a_mswap(it, rit, array->size);
		it += array->size;
		rit -= array->size;
	}
}

void* 
array_resize(struct array *array, array_ui new_cap)
{
	void *new_data;

	if (new_cap == 0)
		return NULL;

	/* reallocated the internal block */
	new_data = ARRAY_REALLOC(array->data, new_cap * array->size);
	if (new_data == NULL)
		return NULL;

	/* fill in new info */
	array->data = new_data;
	array->cap = new_cap;
	if (new_cap < array->len)
		array->len = new_cap;

	return array;
}

void*
array_extend(struct array *array, array_ui len)
{
	array_ui new_cap;

	new_cap = array->cap + len;
	if (new_cap < array->cap || new_cap < len) {
		errno = EOVERFLOW;

		return NULL;
	}

	return array_resize(array, new_cap);
}

void 
array_sort(struct array *array, array_ui ind, array_ui len, 
		int (*comp_fn)(const void*, const void*))
{
	qsort(array_get(array, ind), len, array->size, comp_fn);
}

static void
la_insort(struct array *array, array_ui ind, array_ui len, 
		int (*comp_fn)(const void*, const void*))
{
	array_ui i, insert_ind;
	array_uc *it;

	it = array->data;

	for (i = 1; i < len; i++) {
		it = array_get(array, ind + i);
		insert_ind = array_ubound(array, it, ind, i, comp_fn);

		array_rotate(array, insert_ind, ind + i - insert_ind + 1, 1);
	}
}

static void
la_merge_in_place(struct array *array, array_ui l, array_ui m, array_ui h,
		int (*comp_fn)(const void*, const void*))
{
	array_ui i, j, ind;
	array_uc *it, *tmp;

	i = m;

	while (i < h) {
		it = array_get(array, i);
		ind = array_ubound(array, it, l, i - l, comp_fn);

		tmp = array_get(array, i);

		j = i + 1;
		while (j < h && comp_fn(tmp, array_get(array, j)) > 0)
			j++;

		array_rotate(array, ind, j - ind, j - i);
		l = ind + (j - i);
		i = j;
	}
}

void
array_ssort(struct array *array, array_ui ind, array_ui len,
		int (*comp_fn)(const void*, const void*))
{
	array_ui i, block_size, block_num, l, m, h, range, end;

	if (len < THRESHOLD) {
		la_insort(array, ind, len, comp_fn);

		return;
	}

	block_size = THRESHOLD;
	block_num = len / block_size;
	end = ind + len;
	if (len % block_size > 0)
		block_num++;

	for (i = 0; i < block_num; i++) {
		l = i * block_size + ind;
		range = block_size;
		if ((end - l) < range)
			range = end - l;

		la_insort(array, l, range, comp_fn);
	}

	while (block_num > 1) {
		range = block_num & ~1;

		for (i = 0; i < range; i += 2) {
			l = i * block_size + ind;
			m = l + block_size;
			h = m + block_size;
			if (end < h)
				h = end;

			if (comp_fn(array_get(array, h - 1), array_get(array, l)) < 0)
				array_rotate(array, l, h - l, h - m);
			else
				la_merge_in_place(array, l, m, h, comp_fn);
		}

		block_num -= range / 2;
		block_size += block_size;
	}
}

array_ui
array_partition(struct array *array, array_ui ind, array_ui len, 
		int (*pred_fn)(const void*))
{
	array_uc *it, *rit;

	/* go the the desired indexes */
	it = array_get(array, ind);
	rit = array_get(array, ind + len);
	rit -= array->size; /* prevent underflow */

	while (1) {
		/* find the element that misses the predicate */
		while (it <= rit && pred_fn(rit) != 0)
			rit -= array->size;

		/* find the element that satisfies the predicate */
		while (it <= rit && pred_fn(it) == 0)
			it += array->size;

		/* break if each pointer crosses each other, else swap data */
		if (it > rit)
			break;
		else
			a_mswap(it, rit, array->size);
	}

	return array_index(array, it);
}

array_ui 
array_spartition(struct array *array, array_ui ind, 
		array_ui len, int (*pred_fn)(const void*))
{
	array_uc *it, *lit, *rit;
	array_sl range_part, range;

	it = lit = array_get(array, ind); /* begin */
	rit = array_get(array, ind + len); /* end */
	range_part = 0; /* how many are partitioned so far */

	while (1) {
		/* range of false items to be moved */
		range = 0;

		/* find and add interleaved true items */
		while (it < rit && pred_fn(it) != 0) {
			it += array->size;
			range_part++;
		}

		/* count false items to be moved */
		while (it < rit && pred_fn(it) == 0) {
			it += array->size;
			range++;
		}

		/* if there's no false item, then rit is reached */
		if (range == 0)
			break;

		/**
		 * move false items down and true items up by rotating 
		 *
		 * array_interpose() also works, but more expensively and not effective
		 * with the current variables setup
		 */
		array_rotate(
				array, 
				array_index(array, lit), 
				range_part + range,
				-range_part);

		/* update the current head of the partitioned items */
		lit = lit + range * array->size;
	}

	return array_index(array, lit);
}

struct array a_array;
int (*a_comp_fn)(const void*, const void*);

/* utility function to test whether a give item already exists in a_array */
int
a_uniq(const array_uc *cd)
{
	ARRAY_FOR(&a_array, i) {
		if (a_comp_fn(array_get(&a_array, i), cd) == 0) {
			return 0;
		}
	}

	return 1;
}

void
array_dedup(struct array *array, array_ui ind, array_ui len,
		int (*comp_fn)(const void*, const void*))
{
	array_uc *it, *lit, *rit;
	array_ui ulen, dlen;

	if (len < 2)
		return ;

	it = array_get(array, ind);
	rit = it + len * array->size;
	a_array.data = it;
	a_array.size = array->size;
	a_array.len = 1;
	a_comp_fn = comp_fn;
	it += array->size;

	/* find the beginning unique stride of items */
	/* break when a duplicate is found */
	while (it < rit && a_uniq(it)) {
		it += array->size;
		a_array.len++;
	}

	/* return when there is no duplicate */
	if (it == rit)
		return ;

	/* number of duplicates so far */
	dlen = 1;

	while (1) {
		/* leverage iterator, used to find duplicate and unique strides */
		lit = it + dlen * array->size;

		/* find the duplicate stride right after iterator it */
		while (lit < rit && !a_uniq(lit)) {
			lit += array->size;
			dlen++;
		}

		/* ran into a unique */
		/* find the unique stride after the previous duplicate stride */
		lit += array->size;
		ulen = 1;
		while (lit < rit && a_uniq(lit)) {
			lit += array->size;
			ulen++;
		}

		if (lit >= rit)
			break;

		/* join the found unique stride with the main unique stride
		 *
		 * the duplicates don't matter anymore as long as their length is known 
		 * for moving memory around
		 */
		memmove(it, it + dlen * array->size, ulen * array->size);

		/* update the iterator it to the end of the main unique stride */
		it = it + ulen * array->size;
		a_array.len += ulen;
	}

	/* delete the duplicates */
	if (a_array.len < len)
		array_delete(array, ind + a_array.len, len - a_array.len);
}

array_ui
array_lbound(struct array *array, const void *data,
	 array_ui ind, array_ui len, int (*comp_fn)(const void*, const void*))
{
	array_ui l, h, m;
	int ret;

	l = ind;
	h = ind + len;

	/* reduce the width until a desired index is met */
	while (l < h) {
		m = (l + h) / 2;
		ret = comp_fn(data, array_get(array, m));
		if (ret > 0)
			l = m + 1;
		else
			h = m;
	}

	return l;
}

array_ui
array_ubound(struct array *array, const void *data, 
	 array_ui ind, array_ui len, int (*comp_fn)(const void*, const void*))
{
	array_ui l, h, m;
	int ret;

	l = ind;
	h = ind + len;

	/* reduce the width until a desired index is met */
	while (l < h) {
		m = (l + h) / 2;
		ret = comp_fn(data, array_get(array, m));
		if (ret >= 0)
			l = m + 1;
		else
			h = m;
	}

	return l;
}

array_ui
array_bsearch(struct array *array, const void *data, 
	array_ui ind, array_ui len, int (*comp_fn)(const void*, const void*))
{
	array_ui l, h, m;
	int ret;

	l = ind;
	h = ind + len;

	/* modified code of K&R bsearch */
	while (l < h) {
		m = (l + h) / 2;
		ret = comp_fn(data, array_get(array, m));

		if (ret < 0)
			h = m;
		else if (ret > 0)
			l = m + 1;
		else
			return m;
	}

	return ind + len;
}

array_ui 
array_lsearch(struct array *array, const void *data, 
		array_ui ind, array_ui len, int (*comp_fn)(const void*, const void*))
{
	array_uc *it, *rit;

	it = array_get(array, ind);
	rit = array_get(array, ind + len);

	while (it < rit) {
		if (comp_fn(data, it) == 0)
			break;

		it += array->size;
	}

	return array_index(array, it);
}

array_ui 
array_find(struct array *array, array_ui ind, array_ui len, 
		int (*pred_fn)(const void*))
{
	array_uc *it, *rit;

	it = array_get(array, ind);
	rit = array_get(array, ind + len);

	while (it < rit) {
		if (pred_fn(it) != 0)
			break;

		it += array->size;
	}

	return array_index(array, it);
}

array_ui
array_rfind(struct array *array, array_ui ind, array_ui len, 
		int (*pred_fn)(const void*))
{
	array_uc *it, *rit;

	it = array_get(array, ind);
	rit = array_get(array, ind + len);
	it -= array->size;
	rit -= array->size;

	while (it < rit) {
		if (pred_fn(rit) != 0)
			break;

		rit -= array->size;
	}

	return (rit != it) ? array_index(array, rit) : ind + len;
}

void 
array_write(struct array *array, const void *data, array_ui ind, 
		array_ui len)
{
	array_uc *it;

	/* go to the desired index */
	it = array_get(array, ind);

	/* set the requested block to 0 if data is NULL */
	if (data == NULL) {
		memset(it, 0, len * array->size);

		return;
	}

	/* else overwrite the requested block with new data */
	memcpy(it, data, len * array->size);
}

void 
array_rwrite(struct array *array, const void *data, array_ui ind, 
		array_ui len)
{
	array_uc *it;
	array_ui i, j;

	/* go to the desired index */
	it = array_get(array, ind);

	/* set the requested block to 0 if data is NULL */
	if (data == NULL) {
		memset(it, 0, len * array->size);

		return;
	}

	/* write data repeated */
	memcpy(it, data, array->size);
	j = len - 1;

	for (i = 1; i < j; i *= 2) {
		memcpy(it + i * array->size, it, i * array->size);
		j -= i;
	}

	memcpy(it + (len - j) * array->size, it, j * array->size);
}

int
array_sorted(struct array *array, array_ui ind, array_ui len,
		int (*comp_fn)(const void*, const void*))
{
	array_uc *it, *rit;

	it = array_get(array, ind);
	rit = array_get(array, ind + len);
	rit -= array->size; /* prevent underflow */

	while (it < rit) {
		if (comp_fn(it, it + array->size) > 0)
			return 0;
		it += array->size;
	}

	return 1;
}

void 
array_clear(struct array *array)
{
	ARRAY_FREE(array->data);
}

void 
array_free(struct array *array)
{
	array_clear(array);
	ARRAY_FREE(array);
}

#ifdef ARRAY_DEBUG
void
array_print(struct array *array, array_ui ind, array_ui len,
		const char *delimiter, void (*display)(const void *))
{
	array_uc *it, *end;

	if (delimiter == NULL)
		delimiter = "\n";

	it = array_get(array, ind);
	end = it + array->size * len;
	
	while (it < end) {
		display(it);

		if (it < end - array->size)
			printf("%s", delimiter);

		it += array->size;
	}

	puts("");
}
#endif /* ARRAY_DEBUG */

